build:
	runestone build

run:
	runestone serve 

clean:
	rm -rf build/

init:
	pip install -r requirements.txt

.PHONY: build
