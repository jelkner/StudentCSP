..  Copyright (C)  Mark Guzdial, Barbara Ericson, Briana Morrison, Jeff Elkner
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3 or
    any later version published by the Free Software Foundation; with
    Invariant Sections being Forward, Prefaces, and Contributor List,
    no Front-Cover Texts, and no Back-Cover Texts.  A copy of the license
    is included in the section entitled "GNU Free Documentation License".

..  shortname:: Chapter: What You Can Do with a Computer
..  description:: Some tidbits of what you can do with a computer

.. setup for automatic question numbering.

.. qnum::
    :start: 1
    :prefix: csp-1-7-


.. |runbutton| image:: Figures/run-button.png
    :height: 20px
    :align: top
    :alt: run button

.. |audiobutton| image:: Figures/start-audio-tour.png
    :height: 20px
    :align: top
    :alt: audio tour button


Standards [#f1]_
================

Big Ideas
---------

This book will address the following big ideas from Computer Science:

Big Idea 1: Creative Development
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* 1.1 Collaboration
* 1.2 Program Function and Purpose 
* 1.3 Program Design and Development 
* 1.4 Identifying and Correcting Errors 

Big Idea 2: Data 
~~~~~~~~~~~~~~~~

* 2.1 Binary Numbers 
* 2.2 Data Compression 
* 2.3 Extracting Information from Data
* 2.4 Using Programs with Data
 
Big Idea 3: Algorithms and Programming 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* 3.1 Variables and Assignments 
* 3.2 Data Abstraction 
* 3.3 Mathematical Expressions
* 3.4 Strings
* 3.5 Boolean Expressions
* 3.6 Conditionals
* 3.7 Nested Conditionals
* 3.8 Iteration
* 3.9 Developing Algorithms
* 3.10 Lists
* 3.11 Binary Search
* 3.12 Calling Functions (a.k.a. Procedures)
* 3.13 Developing Functions (Procedures)
* 3.14 Libraries
* 3.15 Simulation
* 3.16 Algorithmic Efficiency
* 3.17 Undecidable Problems
 
Big Idea 4: Computer Systems and Networks 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* 4.1 The Internet
* 4.2 Fault Tolerance
* 4.3 Parallel and Distributed Computing
 
Big Idea 5: Impact of Computing 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* 5.1 Beneficial and Harmful Effects 
* 5.2 Digital Divide
* 5.3 Computing Bias
* 5.4 Crowdsourcing
* 5.5 Legal and Ethical Concerns
* 5.6 Safe Computing


Computational Thinking: Practices and Skills
--------------------------------------------

This book will introduce the following computational thinking practices and
skills:

Practice 1: Computational Solution Design
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Design and evaluate computational solutions for a purpose.

* 1.A Investigate the situation, context, or task.
* 1.B Determine and design an appropriate method or approach to achieve the
  purpose.
* 1.C Explain how collaboration affects the development of a solution.
* 1.D Evaluate solution options.

Practice 2: Algorithms and Program Development
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Develop and implement algorithms.

* 2.A Represent algorithmic processes without using a programming language.
* 2.B Implement and apply an algorithm.

Practice 3: Abstraction in Program Development
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Develop programs that incorporate abstractions.

* 3.A Generalize data sources through variables.
* 3.B Use abstaction to manage complexity in a program.
* 3.C Explain how abstraction manages complexity.

Practice 4: Code Analysis
~~~~~~~~~~~~~~~~~~~~~~~~~

Evaluate and text algorithms and programs.

* 4.A Explain how a code segment or program functions.
* 4.B Determine the result of code segments.
* 4.C Identify and correct errors in algorithms and programs, including
  error discovery through testing.

Practice 5: Computing Innovations
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Investigate computing innovations.

* 5.A Explain how computing systems work.
* 5.B Explain how knowledge can be generated from data.
* 5.C Describe the impact of a computing innovation.
* 5.D Describe the impact of gathering data.
* 5.E Evaluate the use of computing based on legal and ethical factors.

Practice 6: Responsible Computing
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Contribute to an inclusive, safe, collaborative, and ethical computing culture.

* 6.A Collaborate in the development of solutions.
* 6.B Use safe and secure methods when using computing devices.
* 6.C Acknowledge the intellectual property of others. [#f2]_

This chapter should have given you a sense for what we are going to be doing
with a computer in this book.  Let's get started in the next chapter by talking
about what the computer can do and how you can control it.  


.. rubric:: Footnotes

.. [#f1] These standards were developed as part of the `AP Computer Science
   Principles <https://en.wikipedia.org/wiki/AP_Computer_Science_Principles>`_
   curriculum.
.. [#f2] There are some who take issue with the very notion of "intellectual
   property" (see `"Copyright vs. Community" and "Don't Even Think
   "Intellectual Property" <https://www.fsf.org/events/20071026nagoya>`_), so
   we will discuss the current state of law regarding software copyright
   licenses and patents instead.
