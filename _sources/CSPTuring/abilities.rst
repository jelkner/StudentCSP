..  Copyright (C)  Mark Guzdial, Barbara Ericson, Briana Morrison, Jeff Elkner
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3 or
    any later version published by the Free Software Foundation; with
    Invariant Sections being Forward, Prefaces, and Contributor List,
    no Front-Cover Texts, and no Back-Cover Texts.  A copy of the license
    is included in the section entitled "GNU Free Documentation License".

..  description:: Describes what computers can do.

.. setup for automatic question numbering.

.. 	qnum::
	:start: 1
	:prefix: csp-2-3-

Computer Abilities
==================

In this book, we are going to talk about what a computer can do in terms of the
programming language **Python**.  We are not going to cover *all* of Python.
We are going to describe the four abilities of a computer, the four abilities
that cover *everything* that a computer can do.

- Computers can receive data as **input**.
- Computers can **store** data. 
- Computers can **process** data (by *computing* with it). 
- Computers can send data as **output**. 

This *input*, *storage*, *processing*, *output* describes the basic functions
of all computers.

Computers store all data, numbers, words, images, turtle instructions, etc., as
a sequence of `bits <https://en.wikipedia.org/wiki/Bit>`_, which are
interpreted by the computer in different ways depending on the type of data.
