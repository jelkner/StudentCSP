..  Copyright (C)  Mark Guzdial, Barbara Ericson, Briana Morrison, Jeff Elkner
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3 or
    any later version published by the Free Software Foundation; with
    Invariant Sections being Forward, Prefaces, and Contributor List,
    no Front-Cover Texts, and no Back-Cover Texts.  A copy of the license
    is included in the section entitled "GNU Free Documentation License".
    
.. |runbutton| image:: Figures/run-button.png
    :height: 20px
    :align: top
    :alt: run button

.. |audiobutton| image:: Figures/start-audio-tour.png
    :height: 20px
    :align: top
    :alt: audio tour button

.. qnum::
    :start: 1
    :prefix: csp-4-2-

.. index::
    single: dot-notation
    pair: programming; dot-notation

.. _stringsRobjects:

Strings are Objects
===================
   
Strings are objects in Python which means that there is a set of built-in
functions that you can use to manipulate strings.  You use **dot-notation** to
invoke the functions on a string object such as ``sentence.lower()``.  The
function ``lower()`` returns a new string with all of the characters in the
original string set to lowercase.  The function ``capitalize()`` will return a
new string with the first letter of the string capitalized.

.. activecode:: String_Methods2
   :tour_1: "Line-by-line Tour"; 1: str2-line1; 2: str2-line2; 3: str2-line3; 4: str2-line4; 5: str2-line5;
   :nocodelens:
   
   sentence = "THIS IS A TEST"
   better = sentence.lower()
   print(better)
   better_still = better.capitalize() + "."
   print(better_still)
   
   
Getting Part of a String
------------------------

.. index::
    single: index
    single: slice
    pair: string; slice

A string is a sequence of characters.  Each character can be referenced by its
**index**, which is a number between 0 and one less than the total number of
characters in the string as shown below.

.. figure:: Figures/stringIndicies.png
    :width: 400px
    :align: center
    :alt: a string with the position (index) shown above each character
    :figclass: align-center

    Figure 1: A string with the position (index) shown above each character

The index gives the position of the character in the string.

.. note::

   In many languages characters and strings are different *data types*, but
   in Python they are not.  A character is just a string of length 1.

.. activecode:: String_Index1_a
    :nocodelens:

    print("Python"[0])
    print("Computer Science"[9])


.. index::
    single: find
    pair: string; find

Finding a Small String in a Bigger One
--------------------------------------

The ``string.find(part)`` function takes a part of a string (often a single
character) as input and returns the index of the first place in the string
where the part begins. If the part isn't found, ``find`` returns -1.

.. activecode:: String_Find_a
    :nocodelens:

    sentence = "This is a test"
    pos = sentence.find("a")
    print(pos)
    pos = sentence.find("is")
    print(pos)
    pos = sentence.find("love")
    print(pos)

.. note::

    If a string has more than one occurance of the part, there is an *optional
    argument* to ``find`` that tells it where to begin searching, so
    ``sentence.find("is", 3)`` will return ``4``, the first occurance of "is"
    found starting the search at index ``3``.
 
.. figure:: Figures/stringIndicies.png
    :width: 400px
    :align: center
    :alt: a string with the position (index) shown above each character
    :figclass: align-center

    Figure 2: A string with the position (index) shown above each character

A **slice** is a way to get part of a string. It uses the same *square
brackets* that an index uses, but it has two integer values inside with a ``:``
between them that tell where the part begins and ends.

The slice will include all the characters from the first index up to *but
not including* the second index.

.. activecode:: String_Slice2
    :nocodelens:
   
    sentence = "This is a test"
    s1 = sentence[1:3]
    print(s1)
    s2 = sentence[5:7]
    print(s2)
    s3 = sentence[8:11]
    print(s3)
    s4 = sentence[10:14]
    print(s4)
   
**Check Your Understanding**

.. mchoice:: 4_2_1_Slice
    :answer_a: This is the end
    :answer_b: This
    :answer_c: his
    :correct: c
    :feedback_a: This would be true if we were printing the value of str and we hand't changed it on line 2.
    :feedback_b: This would be true if the first position was 1 and the substring included the character at the end position, but the first character in a string is at position 0 and the substring won't include the character at the last position.
    :feedback_c: This will return a string that starts at position 1 and ends at position 3.  

    What will be printed when the following executes?
   
    :: 

        str = "This is the end"
        str = str[1:4]
        print(str)


.. mchoice:: 4_2_2_Slice2
    :answer_a: i
    :answer_b: s
    :answer_c: is the end
    :correct: a
    :feedback_a: This will print the character at position 5 in the string which is i.  Remember that the first character is at position 0.
    :feedback_b: This would be true if the first character was at position 1 instead of 0.
    :feedback_c: This would be true if it returned from the given position to the end of the string, but that isn't what it does.

    What will be printed when the following executes?
   
    :: 

        str = "This is the end"
        str = str[5]
        print(str)


.. index::
    single: len
    pair: string; len

Getting the Length of a String
------------------------------

The ``len(string)`` function takes a string as input and returns the number of
characters in the string including spaces. 

.. activecode:: String_Length
    :nocodelens:

    sentence = "This is a test"
    length = len(sentence)
    print(length)
    sentence = "Hi"
    length = len(sentence)
    print(length)
  
**Check your understanding**

.. mchoice:: 4_2_3_stringLen
    :answer_a: 13
    :answer_b: 15
    :answer_c: 10
    :correct: b
    :feedback_a: Don't forget to include the spaces in the count.
    :feedback_b: The len function returns the number of elements in the string including spaces.
    :feedback_c: This would be true if the len function only returned the number of alphabetic characters, but it includes all including spaces.

    Given the following code segment, what will be printed?
   
    ::

        street = "125 Main Street"
        print(len(street))
     
.. mchoice:: 4_2_4_Find
    :answer_a: 1
    :answer_b: 9
    :answer_c: 10
    :correct: a
    :feedback_a: The find function returns the index of the first position that contains the given string.
    :feedback_b: This would be true if it was pos = str.find(" is").
    :feedback_c: This would be true if it was pos = str.find(" is") and the first position was 1, but it is 0.

    What will be printed when the following executes?
   
    :: 

        str = "His shirt is red"
        pos = str.find("is")
        print(pos)

.. tabbed:: 4_2_5_WSt

    .. tab:: Question

        Write the code to evaluate the length of the string "I like green eggs"
        and print it. It should print "The length is 17".
 
        .. activecode::  4_2_5_WSq
            :nocodelens:

    .. tab:: Answer
        
        Create variable to hold the string.  
            
        .. activecode::  4_2_5_WSa
            :nocodelens:

            # Declare variables
            sentence = 'I like green eggs'
            # Print result 
            print('The length is ' + str(len(sentence)))
