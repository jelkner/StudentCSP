..  Copyright (C)  Brad Miller, David Ranum, Jeff Elkner, Peter Wentworth,
    Allen B. Downey, Chris Meyers, and Dario Mitchell.  Permission is granted
    to copy, distribute and/or modify this document under the terms of the GNU
    Free Documentation License, Version 1.3 or any later version published by
    the Free Software Foundation; with Invariant Sections being Forward,
    Prefaces, and Contributor List, no Front-Cover Texts, and no Back-Cover
    Texts.  A copy of the license is included in the section entitled "GNU Free
    Documentation License".


.. setup for automatic question numbering.

.. qnum::
    :start: 1
    :prefix: 15-6-

Chapter 15 Exercises
---------------------

Below is a selection of images that you can use in the programs in this
section.

.. raw:: html

   <table>
   <tr><td>beach.jpg</td><td>baby.jpg</td><td>vangogh.jpg</td><td>swan.jpg</td></tr>
   <tr><td><img src="../_static/beach.jpg" id="beach.jpg"></td><td><img src="../_static/baby.jpg" id="baby.jpg"></td><td><img src="../_static/vangogh.jpg" id="vangogh.jpg"></td><td><img src="../_static/swan.jpg" id="swan.jpg"></td></tr>
   </table>
   <table>
   <tr><td>puppy.jpg</td><td>kitten.jpg</td><td>girl.jpg</td><td>motorcycle.jpg</td></tr>
   <tr><td><img src="../_static/puppy.jpg" id="puppy.jpg"></td><td><img src="../_static/kitten.jpg" id="kitten.jpg"></td><td><img src="../_static/girl.jpg" id="girl.jpg"></td><td><img src="../_static/motorcycle.jpg" id="motorcycle.jpg"></td></tr>
   </table>
   <table>
   <tr><td>gal1.jpg</td><td>guy1.jpg</td><td>gal2.jpg</td></tr>
   <tr><td><img src="../_static/gal1.jpg" id="gal1.jpg"></td><td><img src="../_static/guy1.jpg" id="guy1.jpg"></td><td><img src="../_static/gal2.jpg" id="gal2.jpg"></td></tr>
   </table>

.. note::

   Remember that it can take a bit of time to process all the pixels in a
   picture!  Check for errors below the code if it is taking a long time, but
   if you don't see any errors just wait.

#.
    .. tabbed:: ch15ex1t

        .. tab:: Question

            Make changes to 10 lines in the code below so that it runs.  It
            changes areas that look red in the original to green.

            .. activecode:: ch15ex1q
                :nocodelens:

                from  import 

                img = Image.open(gal2.jpg')
                pixels = img.load

                for x in range(img.size[]):
                    for y in range(img.size[])
                        r, g, b = pixels
                        if r > 200 and g < 100 and b < 100:
                            pixels[x, y] = 0, g, b

                img.show()

#.
    .. tabbed:: ch15ex2t

        .. tab:: Question

            Fix the code below so that the red in the picture gets changed to
            blue.

            .. activecode::  ch15ex2q
                :nocodelens:

                from PIL import Image 

                img = Image.open('girl.jpg')
                pixels = img.load()

                for x in range(img.size[0]):
                    for y in range(img.size[1]):
                        r, g, b = pixels[x, y]
                        if r < 150 and g > 100 and b > 100:
                            pixels[x, y] = 0, 0, 0

                img.show()

#.
    .. tabbed:: ch15ex3t

        .. tab:: Question

           Fix the indention in the code below so that it runs correctly.  It
           does a primitive form of edge detection by getting all of the pixels
           (except for the last row) and all the pixels to the right of those
           and determining if the difference between the average of the rgb
           values for the pixel and the pixel to the right are substantially
           different.

           .. activecode::  ch15ex3q
                :nocodelens:

                from PIL import Image 

                img = Image.open('swan.jpg')
                pixels = img.load()

                for x in range(img.size[0]-1):
                for y in range(img.size[1]):
                r1, g1, b1 = pixels[x, y]
                r2, g2, b2 = pixels[x + 1, y]
                avg1 = (r1 + g1 + b1) // 3
                avg2 = (r2 + g2 + b2) // 3
                if abs(avg2 - avg1) > 10:
                pixels[x, y] = 0, 0, 0
                else:
                pixels[x, y] = 255, 255, 255 

                img.show()

#.
    .. tabbed:: ch15ex4t

        .. tab:: Question

            The following program has numerous errors, including missing lines,
            improper indentation, and other missing syntax. Fix and change the
            code to change just the background color from white to gray.

            .. activecode::  ch15ex4q
                :nocodelens:

                img = Image.open('gal2.jpg')

                for x in range(img.size[]):
                for y in range(img.size[]):
                r, g, b = pixels

                if r > 250 and g > 250 and b > 250:
                pixels[x, y] = 127, 127, 127 

                img.show()

#.
    .. tabbed:: ch15ex5t

        .. tab:: Question

           Fill in the missing values in the code below so that it runs
           correctly and *posterizes* the picture, which means that it reduces
           the number of colors to a small number, like the ones you might use
           if you were making a poster.

           .. activecode::  ch15ex5q
                :nocodelens:

                from PIL import Image 

                img = Image.open('beach.jpg')
                pixels = img.load()

                for x in range(img.size[0]):
                    for y in range(img.size[1]):
                        r, g, b = pixels[x, y]

                        if r < ??:
                            r = ?? 
                        if r >= ??:
                            r = ??
                        if g < ??:
                            g = ??
                        if g >= ??:
                            g = ?? 
                        if b < ??:
                            b = ??
                        if b >= ??:
                            b = ??

                        pixels[x, y] = r, g, b

                img.show()

#.
    .. tabbed:: ch15ex6t

        .. tab:: Question

            Fix the indentation so that the code puts the motorcycle on the
            beach. The code checks if the pixel isn't white in the first image,
            and if it's not, it places that pixel in the same location on the
            second image.

            .. activecode::  ch15ex6q
                :nocodelens:

                from PIL import Image 

                img1 = Image.open('motorcycle.jpg')
                pixels1 = img1.load()
                w1 = img1.size[0]
                h1 = img1.size[1]

                img2 = Image.open('beach.jpg')
                pixels2 = img2.load()
                w2 = img2.size[0]
                h2 = img2.size[1]

                maxw = min(w1, w2)
                maxh = min(h1, h2)

                for x in range(maxw):
                for y in range(maxh):
                r, g, b = pixels1[x, y]
                if r < 250 and g < 250 and b < 250:
                pixels2[x, y] = r, g, b

                img2.show()

#.

    .. tabbed:: ch15ex7t

        .. tab:: Question

           Fix 6 errors in the code below to copy the non-white pixels from
           gal1.jpg to guy1.jpg.

           .. activecode::  ch15ex7q
                :nocodelens:

                from PIL import Image 

                img1 = Image.open('gal1.jpg')
                pixels1 = img1.load()

                img2 = Image.open(guy1.jpg')
                pixels2 = img2.load(

                for x in range(img1.size[0]:
                    for y in range(img1.size[1])
                        r, g, b = pixels1[x, ]

                        if r < 250 and g < 250 and b < 250:
                            pixels2[ , y] = r g, b

                img2.show()

#.
    .. tabbed:: ch15ex8t

        .. tab:: Question

            The code below was intended to display a swan on the beach, but it
            isn't working.  Find and fix a single error to make it work. 

            .. activecode::  ch15ex8q
                :nocodelens:

                from PIL import Image

                img1 = Image.open('swan.jpg')
                img2 = Image.open('beach.jpg')
                pixels1 = img1.load()
                pixels2 = img2.load()

                w1 = img1.size[0]
                h1 = img1.size[1]
                w2 = img2.size[0]
                h2 = img2.size[1]

                maxw = min(w1, w2)
                maxh = min(h1, h2)

                for x in range(maxw):
                    for y in range(maxh):
	                r, g, b = pixels1[x, y]
	                if r > 100 and g > 100 and b > 100:
	                    pixels1[x, y] = r, g, b

                img2.show()

#.
    .. tabbed:: ch15ex9t

        .. tab:: Question

            Change the code below to use ``if`` and ``else`` rather than
            two ``if`` statements per color to *posterize* the image.

            .. activecode::  ch15ex9q
                :nocodelens:

                from PIL import Image 

                img = Image.open('beach.jpg')
                pixels = img.load()

                for x in range(img.size[0]):
                    for y in range(img.size[1]):
                        r, g, b = pixels[x, y]

                        # Posterize colors 
                        if r < 120:
                            r = 0
                        if r >= 120:
                            r = 120
                        if g < 120:
                            g = 0
                        if g >= 120:
                            g = 120
                        if b < 120:
                            b = 0
                        if b >= 120:
                            b = 120

                        pixels[x, y] = r, g, b

                img.show()

#.
    .. tabbed:: ch15ex10t

        .. tab:: Question

            Fix the indentation in the code and change it so that it edges the
            motorcycle but the background is black and the motorcycle edging
            is white.

            .. activecode::  ch15ex10q
                :nocodelens:

                from PIL import Image 

                img = Image.open('motorcycle.jpg')
                pixels = img.load()

                # Loop through all but last column
                for x in range(img.size[0] - 1):
                for y in range(img.size[1]):
                r1, g1, b1 = pixels[x, y]
                avg1 = (r1 + g1 + b1) / 3
                r2, g2, b2 = pixels[x + 1, y]
                avg2 = (r2 + g2 + b2) / 3

                # Set value for the new color
                if abs(avg2 - avg1) > 10:
                pixels[x, y] = 0, 0, 0
                else:
                pixels[x, y] = 255, 255, 255 

                img.show()

#.
    .. tabbed:: ch15ex11t

        .. tab:: Question

           Change the following code into a procedure that posterizes an image
           that is passed to the procedure.  Be sure to call it to test it.

           .. activecode::  ch15ex11q
                :nocodelens:

                from PIL import Image 

                img = Image.open('beach.jpg')
                pixels = img.load()

                for x in range(img.size[0]):
                    for y in range(img.size[1]):
                        r, g, b = pixels[x, y]

                        # Posterize the colors 
                        if r < 120:
                            r = 0
                        else:
                            r = 120
                        if g < 120:
                            g = 0
                        else:
                            g = 120
                        if b < 120:
                            b = 0
                        else:
                            b = 120

                        pixels[x, y] = r, g, b

                img.show()

#.
    .. tabbed:: ch15ex12t

        .. tab:: Question

            Fix the errors in the following code so that it displays an *edged*
            motorcycle, which means the displayed image should only have 2
            colors. The motorcycle should be one color, everything else should
            be the other color.

            .. activecode::  ch15ex12q
                :nocodelens:

                from PIL import Image 


                def edger(img):
                    pixels = img.load()

                    for x in range(img.size[0] ):
                        for y in range(img.size[1]):
                            r1, g1, b1 = pixels[x, y]
                            avg1 = (r1 + g1 + b1) / 3
                            r2, g2, b2 = pixels[x + 1, y]
                            avg2 = (r2 + g2 + b2) / 3

                            # Posterize color 
                            if abs(avg2 - avg1) > 10:
                                pixels[x, y] = 0, 0, 0
                            else:
                                pixels[x, y] = 255, 255, 255 


                    motorcycle_img = Image.open(motorcycle.jpg)
                    edger(motorcycle_img)

#.
    .. tabbed:: ch15ex13t

        .. tab:: Question

           Change the following into a procedure. It changes areas that are
           mostly red looking to green.  Be sure to call it to test it.

           .. activecode::  ch15ex13q
                :nocodelens:

                from PIL import Image 

                img = Image.open('gal2.jpg')
                pixels = img.load()

                for x in range(img.size[0]):
                    for y in range(img.size[1]):
                        r, g, b = pixels[x, y]

                        # Identify redish pixels 
                        if r > 200 and g < 100 and b < 100:

                            # Remove the red
                            pixels[x, y] = 0, g, b

                img.show()

#.
    .. tabbed:: ch15ex14t

        .. tab:: Question

            The code below currently makes the picture mostly gray. Change it
            so that it *posterizes* (reduce the number of colors) the image
            instead.

            .. activecode::  ch15ex14q
                :nocodelens:

                from PIL import Image 

                img = Image.open('kitten.jpg')
                pixels = img.load()

                for x in range(img.size[0]):
                    for y in range(img.size[1]):
                        r, g, b = pixels[x, y]

                        if r < 120:
                            r = 150
                        else:
                            r = 200
                        if g < 120:
                            g = 150
                        else:
                            g = 200
                        if b < 120:
                            b = 150
                        else:
                            b = 200

                        pixels[x, y] = r, g, b

                img.show()

#.
    .. tabbed:: ch15ex15t

        .. tab:: Question

           Write the code to posterize a picture but use 3 values for each
           color instead of 2.  Use 0 if the current value is less than 85, use
           85 if the value is less than 170, else use 170.

           .. activecode::  ch15ex15q
                :nocodelens:

#.
    .. tabbed:: ch15ex16t

        .. tab:: Question

            Fix the errors in the code and change the code to use if's and
            else's instead of just if's.

            .. activecode::  ch15ex16q
                :nocodelens:

                from PIL import Image 

                img = Image.open('gal1.jpg')
                pixels = img.load()

                for x in range(img.size[0])
                    for y in range(img.size[1]):
                        r, g, b = pixels[x y] 

                        if r < 120:
                            r = 0
                        if r >= 120:
                            r = 120
                        if g < 120:
                            g = 0
                        if g >= 120:
                            g = 120
                        if b < 120:
                            b = 0
                        if b >= 120:
                            b = 120

                        pixels[x, y] = r, g, b

                img.show()

#.

    .. tabbed:: ch15ex17t

        .. tab:: Question

           Write the code to do edge detection on a picture, but compare the curent pixel with the one below it rather than the one to the right.

           .. activecode::  ch15ex17q
                :nocodelens:

#.

    .. tabbed:: ch15ex18t

        .. tab:: Question

            Write a procedure that takes an image as a parameter and edges it using the colors blue and white.

            .. activecode::  ch15ex18q
                :nocodelens:

#.

    .. tabbed:: ch15ex19t

        .. tab:: Question

           Write a procedure to remove the red on very red pixels (pixels that have a red value greater than 200 and a green and blue value of less than 100).

           .. activecode::  ch15ex19q
               :nocodelens:

#.

    .. tabbed:: ch15ex20t

        .. tab:: Question

            Write a procedure that takes a picture as a parameter and converts all the red to grayscale.

            .. activecode::  ch15ex20q
                :nocodelens:
