..  Copyright (C)  Mark Guzdial, Barbara Ericson, Briana Morrison, Jeff Elkner
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3 or
    any later version published by the Free Software Foundation; with
    Invariant Sections being Forward, Prefaces, and Contributor List,
    no Front-Cover Texts, and no Back-Cover Texts.  A copy of the license
    is included in the section entitled "GNU Free Documentation License".

.. qnum::
    :start: 1
    :prefix: csp-15-2-

Combining Pictures
===================

We can use a conditional to copy just the non-white pixels from one picture to
another picture.  We can take this tiny image of the women and put her by the
Eiffel tower in Paris, France.  

.. raw:: html

    <img src="../_static/lady_tiny.png" id="lady_tiny.png">
    <img src="../_static/eiffel.jpg" id="eiffel.jpg">
    
.. tabbed:: tab_combine

    .. tab:: Copy_Non_White_Exercize
    
       Run the program below.  What would happen if ``img1`` was wider or
       taller than ``img2``, like if we tried to do this with the apple (see
       below) as img1 and gal2 (see below) as img2?  Can you modify the program
       below to work even if that were true?  One thing you might need to know
       is that the function ``min(value1,value2)`` will return the smaller of
       the two values.  If you have trouble figuring out a solution click on
       the Answer tab to see one way to do this.

       .. activecode:: Copy_Non_White
          :tour_1: "Structural Tour"; 2: id2a/line2; 5-6: id2a/lines5n6; 13-14: id2a/lines13n14; 18: id2a/line18; 21: id2a/line21; 24: id2a/line24;
          :nocodelens:

          # Import Image class from PIL library
          from PIL import Image
    
          # Connect to the image files 
          img1 = Image.open('lady_tiny.png')
          img2 = Image.open('eiffel.jpg')

          # Load the pixels
          pixels1 = img1.load()
          pixels2 = img2.load()

          # Loop through all the pixels in img1
          for x in range(img1.size[0]):
              for y in range(img1.size[1]):
                  r, g, b = pixels1[x, y]
  
                  # Check if the pixel isn't white
                  if r < 250 and g < 250 and b < 250:
            
            	      # Copy the color to img2 
            	      pixels2[x, y + 130] = r, g, b 
            
          # Show the changed image
          img2.show()
        
         
    Below is a selection of images that you can use in the programs in this
    section.

Here are a couple of other pictures that we can also use.  The first is
apple.jpg and the second is gal2.jpg.  The apple is 500 wide by 334 high and
gal2 is 248 wide by 240 high.

.. raw:: html

    <img src="../_static/apple.jpg" id="apple.jpg">
    <img src="../_static/gal2.jpg" id="gal2.jpg">

.. tabbed:: 15_2_1_WSt

        .. tab:: Question

           The decimal red-green-blue color code for purple is 128, 0, 128
           respectively. Write code to change the white background in gal2.jpg
           to purple. 
           
           .. activecode::  15_2_1_WSq
               :nocodelens:

        .. tab:: Answer
            
          .. activecode::  15_2_1_WSa
              :nocodelens:

              from PIL import Image

              img = Image.open('gal2.jpg')
              pixels = img.load()

              for x in range(img.size[0]):
                  for y in range(img.size[1]):
                      r, g, b = pixels[x, y]

                      if r > 250 and g > 250 and b > 250:
                          pixels[x, y] = 128, 0, 128

              img.show()
