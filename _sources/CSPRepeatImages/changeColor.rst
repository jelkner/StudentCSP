..  Copyright (C)  Mark Guzdial, Barbara Ericson, Briana Morrison, Jeff Elkner
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3 or
    any later version published by the Free Software Foundation; with
    Invariant Sections being Forward, Prefaces, and Contributor List,
    no Front-Cover Texts, and no Back-Cover Texts.  A copy of the license
    is included in the section entitled "GNU Free Documentation License".

    
.. |audiobutton| image:: Figures/start-audio-tour.png
    :height: 20px
    :align: top
    :alt: audio tour button

.. qnum::
    :start: 1
    :prefix: csp-11-4-

Changing Step 5: Increasing and decreasing color values
========================================================

Below is a selection of images that you can use in the programs in this section.
	
.. raw:: html

   <table>
   <tr><td>beach.jpg</td><td>baby.jpg</td><td>vangogh.jpg</td><td>swan.jpg</td></tr>
   <tr><td><img src="../_static/beach.jpg" id="beach.jpg"></td><td><img src="../_static/baby.jpg" id="baby.jpg"></td><td><img src="../_static/vangogh.jpg" id="vangogh.jpg"></td><td><img src="../_static/swan.jpg" id="swan.jpg"></td></tr>
   </table>
   <table>
   <tr><td>puppy.jpg</td><td>kitten.jpg</td><td>girl.jpg</td><td>motorcycle.jpg</td></tr>
   <tr><td><img src="../_static/puppy.jpg" id="puppy.jpg"></td><td><img src="../_static/kitten.jpg" id="kitten.jpg"></td><td><img src="../_static/girl.jpg" id="girl.jpg"></td><td><img src="../_static/motorcycle.jpg" id="motorcycle.jpg"></td></tr>
   </table>
   <table>
   <tr><td>gal1.jpg</td><td>guy1.jpg</td><td>gal2.jpg</td></tr>
   <tr><td><img src="../_static/gal1.jpg" id="gal1.jpg"></td><td><img src="../_static/guy1.jpg" id="guy1.jpg"></td><td><img src="../_static/gal2.jpg" id="gal2.jpg"></td></tr>
   </table>

First example: Let's change Step 5, so that we set the green and blue values of
each pixel equal to its red value.

.. activecode:: Image_Decrease_Red
    :tour_1: "Important Lines Tour"; 2: timg5/line2; 5: timg5/line5; 8: timg5/line8; 11-12: timg5/lines11n12; 15: timg5/line15; 18: timg5/line18; 21: timg5/line21;
    :nocodelens: 

    # Step 1: Use the image library 
    from PIL import Image 
    
    # Step 2: Connect to the image
    img = Image.open("gal2.jpg")

    # Step 3: Load the pixels
    pixels = img.load()

    # Step 4: Loop through the pixels
    for col in range(img.size[0]):
        for row in range(img.size[1]):
        
            # Step 5: Get the data
            r, g, b = pixels[col, row]

            # Step 6: Modify the color
            pixels[col, row] = (r, r, r)

    # Step 7: Show the result
    img.show()

Try the program above on some of the other images by changing the name of the
image file on line 5.  What effect does it always have?  Is this what you
expected when we set all three colors to the red value? Why does the image
appear in black-and-white instead of color?

Change line 5 so that all three color values to the intial green value instead
of the red. How does the resulting image differ? Finally, try this again
setting all the colore to the blue value.

We can increase and decrease the color values for a wide range of effects.

Now let's set the red value only to the average of the previous red, green, and
blue values of the pixel and set the green and blue values to 0. Try to
anticipate what the image will look like before running the program.

.. activecode:: Image_Increase_Red
    :nocodelens:

    from PIL import Image 
    
    img = Image.open("gal2.jpg")
    pixels = img.load();

    for col in range(img.size[0]):
        for row in range(img.size[1]):
        
            r, g, b = pixels[col, row]
            # Set red to average of RGB and set green and blue to 0
            pixels[col, row] = ((r + g + b) // 3, 0, 0)

    img.show()

Did it look like you thought it would? Your ability to "be the computer" and
anticipate what a program will do before you run it is a very important skill
for a programmer to develop.

Change the program to set the green pixel to the previous average of all
three colors while setting red and blue to 0.  What happens if you set both
the red and green to the pixel average and leave only the blue at 0?

Take time to experiment with different combinations, and rerun your experiments
with different images.

.. parsonsprob:: Image_Decrease_GB

   Another way to get a similar effect to increasing the red, is to decrease
   the green and blue.  Figure out how to do that in the program above and then
   use that information to drag the code blocks below from the left to the
   right in the correct order with the correct indention. 
   -----
   from PIL import Image 
   =====
   img = Image.open("beach.jpg")
   =====
   pixels = img.load()
   =====
   for col in range(img.size[0]):
   =====
       for row in range(img.size[1]):
   =====
           r, g, b = pixels[col, row] 
   =====       
           pixels[col, row] = (r, g * 0.75, b * 0.75)
   =====
   img.show()

.. tabbed:: 11_4_1_WSt

        .. tab:: Question

           Decrease the red by .5 and increase the blue and green by .5 in puppy.jpg. 
           
           .. activecode::  11_4_1_WSq
               :nocodelens:

        .. tab:: Answer
            
          .. activecode::  11_4_1_WSa
              :nocodelens:

              # Step 1: Use the image library
              from PIL import Image 

              # Step 2: Pick the image
              img = Image.open("puppy.jpg")

              # Step 3: Load the pixels from the image
              pixels = img.load()

              # Step 4: Loop through the pixels
              for col in range(img.size[0]):
                  for row in range(img.size[1]):

                      # Step 5: Get the data
                      r, g, b = pixels[col, row] 

                      # Step 6: Modify the color
                      pixels[col, row] = (r * 0.5, g * 1.5, b * 1.5)

              # Step 7: Show the result
              img.show()
