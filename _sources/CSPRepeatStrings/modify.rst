..  Copyright (C)  Mark Guzdial, Barbara Ericson, Briana Morrison
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3 or
    any later version published by the Free Software Foundation; with
    Invariant Sections being Forward, Prefaces, and Contributor List,
    no Front-Cover Texts, and no Back-Cover Texts.  A copy of the license
    is included in the section entitled "GNU Free Documentation License".
    
.. |audiobutton| image:: Figures/start-audio-tour.png
    :height: 20px
    :align: top
    :alt: audio tour button

.. qnum::
    :start: 1
    :prefix: csp-9-4-
	
.. highlight:: python 
   :linenothreshold: 4

Modifying Text
==============

We can loop through the string and modify it using ``find`` and slice
(substring).  

.. note::

   It might be a good idea to review :ref:`stringsRobjects` before going ahead
   with this section.

Google has been digitizing books.  But, sometimes the digitizer makes a mistake
and uses 1 for i.  The following program replaces ``1`` with ``i`` everywhere
in the string.  Now you might not want to really replace every ``1`` with
``i``, but don't worry about that right now.  It uses a ``while`` loop since we
don't know how many times it will need to loop.

.. codelens:: Change_Ones
    :question: What is the value of pos after the line with the red arrow executes?
    :breakline: 5
    :feedback: Remember that find returns the starting index if the target string is found and -1 if not.  
    :correct: globals.pos
	
    a_str = "Th1s is a str1ng"
    pos = a_str.find("1")
    while pos >= 0:
        a_str = a_str[0:pos] + "i" + a_str[pos+1:len(a_str)]
        pos = a_str.find("1")
    print(a_str)
    
You don't have to replace just one character.  You could replace every instance
of a word with another word.  For example, what if you spelled a word wrong and
wanted to change it everywhere?

.. codelens:: Change_Word
    :question: What is the value of pos after the line with the red arrow executes?
    :breakline: 6
    :feedback: Remember that find returns the starting index if the target string is found and -1 if not.  
    :correct: globals.pos
	
    a_str = "He wanted a peice of candy"
    a_str = a_str + " so he gave her a peice."
    pos = a_str.find("peice")
    while pos >= 0:
        a_str = a_str[0:pos] + "piece" + a_str[pos+len("peice"):len(a_str)]
        pos = a_str.find("peice")
    print(a_str)
    
Can you loop through and encode a string to hide the contents of the message?

.. codelens:: Encode_String
	
    message = "meet me at midnight"
    a_str = "abcdefghijklmnopqrstuvwxyz. "
    e_str = "zyxwvutsrqponmlkjihgfedcba ."
    encoded_message = ""
    for letter in message:
        pos = a_str.find(letter)
        encoded_message = encoded_message + e_str[pos]
    print(encoded_message)


.. mchoice:: 9_4_1_replace_m
   :answer_a: a
   :answer_b: d
   :answer_c: w
   :answer_d: v
   :correct: d
   :feedback_a: The letter in a_str is replaced with the letter at the same position in e_str.
   :feedback_b: The letter in a_str is replaced with the letter at the same position in e_str.
   :feedback_c: Try counting the letters in a_str to figure out how many letters to count in e_str.
   :feedback_d: The letter e is at position 4 in a_str and will be replaced with the letter v at position 4 in e_str.

   What character is e replaced with when the message is encoded?

.. mchoice:: 9_4_2_encodeMess1
   :answer_a: ""
   :answer_b: "o"
   :answer_c: "n"
   :answer_d: "m"
   :correct: c
   :feedback_a: It starts out as the empty string, but a letter is added each time through the loop.
   :feedback_b: The letter in a_str is replaced with the letter at the same position in e_str.
   :feedback_c: The letter in e_str at the same position as the m in a_str is n.
   :feedback_d: Notice that we are adding the letter in e_str at pos not the letter in a_str at pos.

   What is the value of encoded_message after the loop executes the first time?  

.. parsonsprob:: 9_4_3_Decode_String

   The program below decodes an encoded message, but the lines are mixed up.
   Put the lines in the right order with the right indentation.
   -----
   message = ""
   a_str = "abcdefghijklmnopqrstuvwxyz. "
   e_str = "zyxwvutsrqponmlkjihgfedcba ."
   encoded_message = "nvvg.nv.zg.nrwmrtsg"
   =====
   for letter in encoded_message:
   =====
       pos = e_str.find(letter)
   =====
       message = message + a_str[pos]
   =====
   print(message)

.. tabbed:: 9_4_4_WSt

    .. tab:: Question

       Write the code to replace every 0 with o in the given string 'The 0wl
       h00ts l0udly'. 

       .. activecode::  9_4_4_WSq
            :nocodelens:

    .. tab:: Answer

      .. activecode::  9_4_4_WSa
          :nocodelens:
          
          a_str = "The 0wl h00ts l0udly"
          # SET POS TO A VALUE GREATER THAN OR EQUAL TO 0
          pos = 1
          # SET WHILE CONDITION
          while pos >= 0:
              # REPLACE VALUE
              pos = a_str.find("0")
              if pos == -1:
                break
              a_str = a_str[0:pos] + "o" + a_str[pos+1:len(a_str)]
          # PRINT RESULT
          print(a_str)

