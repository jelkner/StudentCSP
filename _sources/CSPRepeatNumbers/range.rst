..  Copyright (C)  Mark Guzdial, Barbara Ericson, Briana Morrison, Jeff Elkner
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3 or
    any later version published by the Free Software Foundation; with
    Invariant Sections being Forward, Prefaces, and Contributor List,
    no Front-Cover Texts, and no Back-Cover Texts.  A copy of the license
    is included in the section entitled "GNU Free Documentation License".

.. qnum::
    :start: 1
    :prefix: csp-7-4-

.. highlight:: python 
    :linenothreshold: 4

The Range Function
==================

You can use the ``range`` function to loop over a sequence of numbers.

If the range function is called with a single positive integer, it will
generate all the integer values from 0 to one less than the number it was
passed and assign them one at a time to the loop variable.

.. activecode:: Numbers_Range1
	
    for number in range(4):
        print(number)
 
.. activecode:: Numbers_Range2
	
    for number in range(8):
        print(number)
 
.. mchoice:: 7_4_1_Numbers_Range1
   :answer_a: range(5)
   :answer_b: range(6)
   :answer_c: range(7)
   :correct: b
   :feedback_a: This will return a the numbers from 0 to 4.
   :feedback_b: This will return a the numbers from 0 to 5.
   :feedback_c: This will return a the numbers from 0 to 6.

   Which of the following calls to the range function will generate the numbers from 0 to 5?

If two integer values are passed as input to the ``range`` function then it
will generate each of values from the first value to *one less than the
second value*.  It is *inclusive* of the first value and *exclusive* of the
second value.

.. activecode:: Numbers_Range3
	
    for number in range(1, 10):
        print(number)
 
.. activecode:: Numbers_Range4
	
    for number in range(0, 11):
        print(number)
 
.. activecode:: Numbers_Range5
	
    for number in range(20, 31):
        print(number)
 
.. mchoice:: 7_4_2_Numbers_Range2
   :answer_a: range(12)
   :answer_b: range(3, 12)
   :answer_c: range(11)
   :answer_d: range(3, 13)
   :correct: d
   :feedback_a: That starts at zero and doesn't include 12.
   :feedback_b: That doesn't include 12.
   :feedback_c: That generates integers from 0 to 10. 
   :feedback_d: That's what we want, integers from 3 to 12. 

   Which of the following calls to range generates integers from 3 to 12?
   
Let's rewrite the program that calculates the product using the ``range``
function to generate the list of numbers as shown below.

.. activecode:: Numbers_Product2
    :tour_1: "Line by line tour"; 1: for2_line1; 2: for2_line2; 3: for2_line3; 4: for2_line4; 5: for2_line5;
	
    product = 1  # Start out with the multiplication identity value 
    numbers = range(1, 11)
    for number in numbers:
    	product = product * number
    print(product)

.. mchoice:: 7_4_3_Numbers_Product_Q3
   :answer_a: 121645100408832000
   :answer_b: 3628800
   :answer_c: 362880
   :answer_d: 2432902008176640000
   :correct: d
   :feedback_a: That is the product of all numbers from 1 to 19 (e.g., you changed the 11 to 20)
   :feedback_b: That is the product of all numbers from 1 to 10 (e.g., no change at all)
   :feedback_c: That is the product of all numbers from 1 to 9 (e.g., you changed the 11 to 10)
   :feedback_d: That is the product of all numbers from 1 to 20 (e.g., you changed the 11 to 21)

   Change *one* number in the above program to tell us the product of all
   integers from 1 to 20

.. tabbed:: 7_4_4_WSt

    .. tab:: Question

        Define a function to calculate the sum of 1 to the passed number using
        the range function.  Return the sum from the function.  Call the
        function and print the result.

        .. activecode::  7_4_4_WSq
            :nocodelens:

    .. tab:: Answer

        .. activecode::  7_4_4_WSa
          :nocodelens:
              
          # Define the function
          def summation(end_value):
            # Initialize accumulator 
            the_sum = 0  
            # Name data
            numbers = range(1, end_value +1)
            # Loop through data
            for number in numbers:
              # Accumulate 
              the_sum = the_sum + number
            # Return sum
            return the_sum

          # Print result
          print(summation(10)) 
